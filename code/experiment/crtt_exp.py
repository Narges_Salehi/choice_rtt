#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2023.2.3),
    on Thu Feb  1 11:24:07 2024
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

import psychopy
psychopy.useVersion('2023.2.3')


# --- Import packages ---
from psychopy import locale_setup
from psychopy import prefs
from psychopy import plugins
plugins.activatePlugins()
prefs.hardware['audioLib'] = 'ptb'
prefs.hardware['audioLatencyMode'] = '3'
from psychopy import sound, gui, visual, core, data, event, logging, clock, colors, layout
from psychopy.tools import environmenttools
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER, priority)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle, choice as randchoice
import os  # handy system and path functions
import sys  # to get file system encoding

import psychopy.iohub as io
from psychopy.hardware import keyboard

# --- Setup global variables (available in all functions) ---
# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
# Store info about the experiment session
psychopyVersion = '2023.2.3'
expName = 'crtt_exp'  # from the Builder filename that created this script
expInfo = {
    'participant': '',
    'session': '',
    'age': '',
    'left-handed': False,
    'Do you like this session?': ['Yes', 'No'],
    'date': data.getDateStr(),  # add a simple timestamp
    'expName': expName,
    'psychopyVersion': psychopyVersion,
}


def showExpInfoDlg(expInfo):
    """
    Show participant info dialog.
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    
    Returns
    ==========
    dict
        Information about this experiment.
    """
    # temporarily remove keys which the dialog doesn't need to show
    poppedKeys = {
        'date': expInfo.pop('date', data.getDateStr()),
        'expName': expInfo.pop('expName', expName),
        'psychopyVersion': expInfo.pop('psychopyVersion', psychopyVersion),
    }
    # show participant info dialog
    dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
    if dlg.OK == False:
        core.quit()  # user pressed cancel
    # restore hidden keys
    expInfo.update(poppedKeys)
    # return expInfo
    return expInfo


def setupData(expInfo, dataDir=None):
    """
    Make an ExperimentHandler to handle trials and saving.
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    dataDir : Path, str or None
        Folder to save the data to, leave as None to create a folder in the current directory.    
    Returns
    ==========
    psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    """
    
    # data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
    if dataDir is None:
        dataDir = _thisDir
    filename = u'data/%s_%s_%s' % (expInfo['participant'], expName, expInfo['date'])
    # make sure filename is relative to dataDir
    if os.path.isabs(filename):
        dataDir = os.path.commonprefix([dataDir, filename])
        filename = os.path.relpath(filename, dataDir)
    
    # an ExperimentHandler isn't essential but helps with data saving
    thisExp = data.ExperimentHandler(
        name=expName, version='',
        extraInfo=expInfo, runtimeInfo=None,
        originPath='/Users/peerherholz/Desktop/choice_rtt/code/experiment/crtt_exp.py',
        savePickle=True, saveWideText=True,
        dataFileName=dataDir + os.sep + filename, sortColumns='time'
    )
    thisExp.setPriority('thisRow.t', priority.CRITICAL)
    thisExp.setPriority('expName', priority.LOW)
    # return experiment handler
    return thisExp


def setupLogging(filename):
    """
    Setup a log file and tell it what level to log at.
    
    Parameters
    ==========
    filename : str or pathlib.Path
        Filename to save log file and data files as, doesn't need an extension.
    
    Returns
    ==========
    psychopy.logging.LogFile
        Text stream to receive inputs from the logging system.
    """
    # this outputs to the screen, not a file
    logging.console.setLevel(logging.EXP)
    # save a log file for detail verbose info
    logFile = logging.LogFile(filename+'.log', level=logging.EXP)
    
    return logFile


def setupWindow(expInfo=None, win=None):
    """
    Setup the Window
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    win : psychopy.visual.Window
        Window to setup - leave as None to create a new window.
    
    Returns
    ==========
    psychopy.visual.Window
        Window in which to run this experiment.
    """
    if win is None:
        # if not given a window to setup, make one
        win = visual.Window(
            size=[1440, 900], fullscr=False, screen=0,
            winType='pyglet', allowStencil=False,
            monitor='testMonitor', color=[0,0,0], colorSpace='rgb',
            backgroundImage='', backgroundFit='none',
            blendMode='avg', useFBO=True,
            units='height'
        )
        if expInfo is not None:
            # store frame rate of monitor if we can measure it
            expInfo['frameRate'] = win.getActualFrameRate()
    else:
        # if we have a window, just set the attributes which are safe to set
        win.color = [0,0,0]
        win.colorSpace = 'rgb'
        win.backgroundImage = ''
        win.backgroundFit = 'none'
        win.units = 'height'
    win.mouseVisible = True
    win.hideMessage()
    return win


def setupInputs(expInfo, thisExp, win):
    """
    Setup whatever inputs are available (mouse, keyboard, eyetracker, etc.)
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    win : psychopy.visual.Window
        Window in which to run this experiment.
    Returns
    ==========
    dict
        Dictionary of input devices by name.
    """
    # --- Setup input devices ---
    inputs = {}
    ioConfig = {}
    
    # Setup iohub keyboard
    ioConfig['Keyboard'] = dict(use_keymap='psychopy')
    
    ioSession = '1'
    if 'session' in expInfo:
        ioSession = str(expInfo['session'])
    ioServer = io.launchHubServer(window=win, **ioConfig)
    eyetracker = None
    
    # create a default keyboard (e.g. to check for escape)
    defaultKeyboard = keyboard.Keyboard(backend='iohub')
    # return inputs dict
    return {
        'ioServer': ioServer,
        'defaultKeyboard': defaultKeyboard,
        'eyetracker': eyetracker,
    }

def pauseExperiment(thisExp, inputs=None, win=None, timers=[], playbackComponents=[]):
    """
    Pause this experiment, preventing the flow from advancing to the next routine until resumed.
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    inputs : dict
        Dictionary of input devices by name.
    win : psychopy.visual.Window
        Window for this experiment.
    timers : list, tuple
        List of timers to reset once pausing is finished.
    playbackComponents : list, tuple
        List of any components with a `pause` method which need to be paused.
    """
    # if we are not paused, do nothing
    if thisExp.status != PAUSED:
        return
    
    # pause any playback components
    for comp in playbackComponents:
        comp.pause()
    # prevent components from auto-drawing
    win.stashAutoDraw()
    # run a while loop while we wait to unpause
    while thisExp.status == PAUSED:
        # make sure we have a keyboard
        if inputs is None:
            inputs = {
                'defaultKeyboard': keyboard.Keyboard(backend='ioHub')
            }
        # check for quit (typically the Esc key)
        if inputs['defaultKeyboard'].getKeys(keyList=['escape']):
            endExperiment(thisExp, win=win, inputs=inputs)
        # flip the screen
        win.flip()
    # if stop was requested while paused, quit
    if thisExp.status == FINISHED:
        endExperiment(thisExp, inputs=inputs, win=win)
    # resume any playback components
    for comp in playbackComponents:
        comp.play()
    # restore auto-drawn components
    win.retrieveAutoDraw()
    # reset any timers
    for timer in timers:
        timer.reset()


def run(expInfo, thisExp, win, inputs, globalClock=None, thisSession=None):
    """
    Run the experiment flow.
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    psychopy.visual.Window
        Window in which to run this experiment.
    inputs : dict
        Dictionary of input devices by name.
    globalClock : psychopy.core.clock.Clock or None
        Clock to get global time from - supply None to make a new one.
    thisSession : psychopy.session.Session or None
        Handle of the Session object this experiment is being run from, if any.
    """
    # mark experiment as started
    thisExp.status = STARTED
    # make sure variables created by exec are available globally
    exec = environmenttools.setExecEnvironment(globals())
    # get device handles from dict of input devices
    ioServer = inputs['ioServer']
    defaultKeyboard = inputs['defaultKeyboard']
    eyetracker = inputs['eyetracker']
    # make sure we're running in the directory for this experiment
    os.chdir(_thisDir)
    # get filename from ExperimentHandler for convenience
    filename = thisExp.dataFileName
    frameTolerance = 0.001  # how close to onset before 'same' frame
    endExpNow = False  # flag for 'escape' or other condition => quit the exp
    # get frame duration from frame rate in expInfo
    if 'frameRate' in expInfo and expInfo['frameRate'] is not None:
        frameDur = 1.0 / round(expInfo['frameRate'])
    else:
        frameDur = 1.0 / 60.0  # could not measure, so guess
    
    # Start Code - component code to be run after the window creation
    
    # --- Initialize components for Routine "welcome" ---
    welcome_message = visual.TextStim(win=win, name='welcome_message',
        text='Welcome to the experiment!\n\nPlease press the space bar to continue.',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    spacebar_welcome = keyboard.Keyboard()
    
    # --- Initialize components for Routine "instructions_general" ---
    general_instructions = visual.TextStim(win=win, name='general_instructions',
        text="In this task, you will make decisions as to which stimulus you have seen. \n\nThere are three versions of the task. In the first, you have to decide which shape you have seen, in the second which image and the third which sound you've heard. \n\nBefore each task, you will get set of specific instructions and short practice period. \n\nPlease press the space bar to continue.",
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    general_instructions_space_bar = keyboard.Keyboard()
    
    # --- Initialize components for Routine "instructions_shape" ---
    instruct_shape = visual.TextStim(win=win, name='instruct_shape',
        text='In this task you will make a decision as to which shape you have seen.\n\nPress C or click cross for a cross\nPress V or click square for a square\nPress B or click plus for a plus\n\nFirst, we will have a quick practice.\n\nPush space bar or click / touch one of the buttons to begin.',
        font='Open Sans',
        pos=(0, 0.1), height=0.035, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    instruct_space_bar = keyboard.Keyboard()
    instruct_shape_square = visual.ImageStim(
        win=win,
        name='instruct_shape_square', 
        image='/Users/peerherholz/Desktop/choice_rtt/stimuli/shapes/response_square.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-2.0)
    instruct_shape_plus = visual.ImageStim(
        win=win,
        name='instruct_shape_plus', 
        image='/Users/peerherholz/Desktop/choice_rtt/stimuli/shapes/response_plus.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    instruct_shape_cross = visual.ImageStim(
        win=win,
        name='instruct_shape_cross', 
        image='/Users/peerherholz/Desktop/choice_rtt/stimuli/shapes/response_cross.jpg', mask=None, anchor='center',
        ori=0.0, pos=(-0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-4.0)
    
    # --- Initialize components for Routine "trial" ---
    visual_reminder_square = visual.ImageStim(
        win=win,
        name='visual_reminder_square', 
        image='/Users/peerherholz/Desktop/choice_rtt/stimuli/shapes/response_square.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-1.0)
    visual_reminder_plus = visual.ImageStim(
        win=win,
        name='visual_reminder_plus', 
        image='/Users/peerherholz/Desktop/choice_rtt/stimuli/shapes/response_plus.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-2.0)
    visual_reminder_cross = visual.ImageStim(
        win=win,
        name='visual_reminder_cross', 
        image='/Users/peerherholz/Desktop/choice_rtt/stimuli/shapes/response_cross.jpg', mask=None, anchor='center',
        ori=0.0, pos=(-0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    left_far_tile = visual.ImageStim(
        win=win,
        name='left_far_tile', 
        image='/Users/peerherholz/Desktop/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-4.0)
    left_mid_tile = visual.ImageStim(
        win=win,
        name='left_mid_tile', 
        image='/Users/peerherholz/Desktop/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-5.0)
    right_mid_tile = visual.ImageStim(
        win=win,
        name='right_mid_tile', 
        image='/Users/peerherholz/Desktop/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-6.0)
    right_far_tile = visual.ImageStim(
        win=win,
        name='right_far_tile', 
        image='/Users/peerherholz/Desktop/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-7.0)
    target_image = visual.ImageStim(
        win=win,
        name='target_image', 
        image='default.png', mask=None, anchor='center',
        ori=0.0, pos=[0,0], size=(0.2, 0.2),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-8.0)
    keyboard_response = keyboard.Keyboard()
    
    # --- Initialize components for Routine "practice_feedback" ---
    feedback_text = visual.TextStim(win=win, name='feedback_text',
        text='',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    keyboard_response_feedback = keyboard.Keyboard()
    
    # --- Initialize components for Routine "end_screen" ---
    end_screen_instructions = visual.TextStim(win=win, name='end_screen_instructions',
        text='You have reached the end of the experiment, thank you very much for participating. \n\nPlease press the space bar to finish.',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    end_screen_space_bar = keyboard.Keyboard()
    
    # create some handy timers
    if globalClock is None:
        globalClock = core.Clock()  # to track the time since experiment started
    if ioServer is not None:
        ioServer.syncClock(globalClock)
    logging.setDefaultClock(globalClock)
    routineTimer = core.Clock()  # to track time remaining of each (possibly non-slip) routine
    win.flip()  # flip window to reset last flip timer
    # store the exact time the global clock started
    expInfo['expStart'] = data.getDateStr(format='%Y-%m-%d %Hh%M.%S.%f %z', fractionalSecondDigits=6)
    
    # --- Prepare to start Routine "welcome" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('welcome.started', globalClock.getTime())
    spacebar_welcome.keys = []
    spacebar_welcome.rt = []
    _spacebar_welcome_allKeys = []
    # keep track of which components have finished
    welcomeComponents = [welcome_message, spacebar_welcome]
    for thisComponent in welcomeComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "welcome" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *welcome_message* updates
        
        # if welcome_message is starting this frame...
        if welcome_message.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            welcome_message.frameNStart = frameN  # exact frame index
            welcome_message.tStart = t  # local t and not account for scr refresh
            welcome_message.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(welcome_message, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'welcome_message.started')
            # update status
            welcome_message.status = STARTED
            welcome_message.setAutoDraw(True)
        
        # if welcome_message is active this frame...
        if welcome_message.status == STARTED:
            # update params
            pass
        
        # *spacebar_welcome* updates
        waitOnFlip = False
        
        # if spacebar_welcome is starting this frame...
        if spacebar_welcome.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            spacebar_welcome.frameNStart = frameN  # exact frame index
            spacebar_welcome.tStart = t  # local t and not account for scr refresh
            spacebar_welcome.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(spacebar_welcome, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'spacebar_welcome.started')
            # update status
            spacebar_welcome.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(spacebar_welcome.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(spacebar_welcome.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if spacebar_welcome.status == STARTED and not waitOnFlip:
            theseKeys = spacebar_welcome.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _spacebar_welcome_allKeys.extend(theseKeys)
            if len(_spacebar_welcome_allKeys):
                spacebar_welcome.keys = _spacebar_welcome_allKeys[-1].name  # just the last key pressed
                spacebar_welcome.rt = _spacebar_welcome_allKeys[-1].rt
                spacebar_welcome.duration = _spacebar_welcome_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in welcomeComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "welcome" ---
    for thisComponent in welcomeComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('welcome.stopped', globalClock.getTime())
    # check responses
    if spacebar_welcome.keys in ['', [], None]:  # No response was made
        spacebar_welcome.keys = None
    thisExp.addData('spacebar_welcome.keys',spacebar_welcome.keys)
    if spacebar_welcome.keys != None:  # we had a response
        thisExp.addData('spacebar_welcome.rt', spacebar_welcome.rt)
        thisExp.addData('spacebar_welcome.duration', spacebar_welcome.duration)
    thisExp.nextEntry()
    # the Routine "welcome" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # --- Prepare to start Routine "instructions_general" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('instructions_general.started', globalClock.getTime())
    general_instructions_space_bar.keys = []
    general_instructions_space_bar.rt = []
    _general_instructions_space_bar_allKeys = []
    # keep track of which components have finished
    instructions_generalComponents = [general_instructions, general_instructions_space_bar]
    for thisComponent in instructions_generalComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "instructions_general" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *general_instructions* updates
        
        # if general_instructions is starting this frame...
        if general_instructions.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            general_instructions.frameNStart = frameN  # exact frame index
            general_instructions.tStart = t  # local t and not account for scr refresh
            general_instructions.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(general_instructions, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'general_instructions.started')
            # update status
            general_instructions.status = STARTED
            general_instructions.setAutoDraw(True)
        
        # if general_instructions is active this frame...
        if general_instructions.status == STARTED:
            # update params
            pass
        
        # *general_instructions_space_bar* updates
        waitOnFlip = False
        
        # if general_instructions_space_bar is starting this frame...
        if general_instructions_space_bar.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            general_instructions_space_bar.frameNStart = frameN  # exact frame index
            general_instructions_space_bar.tStart = t  # local t and not account for scr refresh
            general_instructions_space_bar.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(general_instructions_space_bar, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'general_instructions_space_bar.started')
            # update status
            general_instructions_space_bar.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(general_instructions_space_bar.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(general_instructions_space_bar.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if general_instructions_space_bar.status == STARTED and not waitOnFlip:
            theseKeys = general_instructions_space_bar.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _general_instructions_space_bar_allKeys.extend(theseKeys)
            if len(_general_instructions_space_bar_allKeys):
                general_instructions_space_bar.keys = _general_instructions_space_bar_allKeys[-1].name  # just the last key pressed
                general_instructions_space_bar.rt = _general_instructions_space_bar_allKeys[-1].rt
                general_instructions_space_bar.duration = _general_instructions_space_bar_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in instructions_generalComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "instructions_general" ---
    for thisComponent in instructions_generalComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('instructions_general.stopped', globalClock.getTime())
    # check responses
    if general_instructions_space_bar.keys in ['', [], None]:  # No response was made
        general_instructions_space_bar.keys = None
    thisExp.addData('general_instructions_space_bar.keys',general_instructions_space_bar.keys)
    if general_instructions_space_bar.keys != None:  # we had a response
        thisExp.addData('general_instructions_space_bar.rt', general_instructions_space_bar.rt)
        thisExp.addData('general_instructions_space_bar.duration', general_instructions_space_bar.duration)
    thisExp.nextEntry()
    # the Routine "instructions_general" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # --- Prepare to start Routine "instructions_shape" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('instructions_shape.started', globalClock.getTime())
    instruct_space_bar.keys = []
    instruct_space_bar.rt = []
    _instruct_space_bar_allKeys = []
    # keep track of which components have finished
    instructions_shapeComponents = [instruct_shape, instruct_space_bar, instruct_shape_square, instruct_shape_plus, instruct_shape_cross]
    for thisComponent in instructions_shapeComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "instructions_shape" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *instruct_shape* updates
        
        # if instruct_shape is starting this frame...
        if instruct_shape.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            instruct_shape.frameNStart = frameN  # exact frame index
            instruct_shape.tStart = t  # local t and not account for scr refresh
            instruct_shape.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(instruct_shape, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'instruct_shape.started')
            # update status
            instruct_shape.status = STARTED
            instruct_shape.setAutoDraw(True)
        
        # if instruct_shape is active this frame...
        if instruct_shape.status == STARTED:
            # update params
            pass
        
        # *instruct_space_bar* updates
        waitOnFlip = False
        
        # if instruct_space_bar is starting this frame...
        if instruct_space_bar.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            instruct_space_bar.frameNStart = frameN  # exact frame index
            instruct_space_bar.tStart = t  # local t and not account for scr refresh
            instruct_space_bar.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(instruct_space_bar, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'instruct_space_bar.started')
            # update status
            instruct_space_bar.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(instruct_space_bar.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(instruct_space_bar.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if instruct_space_bar.status == STARTED and not waitOnFlip:
            theseKeys = instruct_space_bar.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _instruct_space_bar_allKeys.extend(theseKeys)
            if len(_instruct_space_bar_allKeys):
                instruct_space_bar.keys = _instruct_space_bar_allKeys[-1].name  # just the last key pressed
                instruct_space_bar.rt = _instruct_space_bar_allKeys[-1].rt
                instruct_space_bar.duration = _instruct_space_bar_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # *instruct_shape_square* updates
        
        # if instruct_shape_square is starting this frame...
        if instruct_shape_square.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            instruct_shape_square.frameNStart = frameN  # exact frame index
            instruct_shape_square.tStart = t  # local t and not account for scr refresh
            instruct_shape_square.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(instruct_shape_square, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'instruct_shape_square.started')
            # update status
            instruct_shape_square.status = STARTED
            instruct_shape_square.setAutoDraw(True)
        
        # if instruct_shape_square is active this frame...
        if instruct_shape_square.status == STARTED:
            # update params
            pass
        
        # *instruct_shape_plus* updates
        
        # if instruct_shape_plus is starting this frame...
        if instruct_shape_plus.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            instruct_shape_plus.frameNStart = frameN  # exact frame index
            instruct_shape_plus.tStart = t  # local t and not account for scr refresh
            instruct_shape_plus.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(instruct_shape_plus, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'instruct_shape_plus.started')
            # update status
            instruct_shape_plus.status = STARTED
            instruct_shape_plus.setAutoDraw(True)
        
        # if instruct_shape_plus is active this frame...
        if instruct_shape_plus.status == STARTED:
            # update params
            pass
        
        # *instruct_shape_cross* updates
        
        # if instruct_shape_cross is starting this frame...
        if instruct_shape_cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            instruct_shape_cross.frameNStart = frameN  # exact frame index
            instruct_shape_cross.tStart = t  # local t and not account for scr refresh
            instruct_shape_cross.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(instruct_shape_cross, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'instruct_shape_cross.started')
            # update status
            instruct_shape_cross.status = STARTED
            instruct_shape_cross.setAutoDraw(True)
        
        # if instruct_shape_cross is active this frame...
        if instruct_shape_cross.status == STARTED:
            # update params
            pass
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in instructions_shapeComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "instructions_shape" ---
    for thisComponent in instructions_shapeComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('instructions_shape.stopped', globalClock.getTime())
    # check responses
    if instruct_space_bar.keys in ['', [], None]:  # No response was made
        instruct_space_bar.keys = None
    thisExp.addData('instruct_space_bar.keys',instruct_space_bar.keys)
    if instruct_space_bar.keys != None:  # we had a response
        thisExp.addData('instruct_space_bar.rt', instruct_space_bar.rt)
        thisExp.addData('instruct_space_bar.duration', instruct_space_bar.duration)
    thisExp.nextEntry()
    # the Routine "instructions_shape" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # set up handler to look after randomisation of conditions etc
    PracticeLoop = data.TrialHandler(nReps=1.0, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=data.importConditions('shapes_targets.csv'),
        seed=None, name='PracticeLoop')
    thisExp.addLoop(PracticeLoop)  # add the loop to the experiment
    thisPracticeLoop = PracticeLoop.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisPracticeLoop.rgb)
    if thisPracticeLoop != None:
        for paramName in thisPracticeLoop:
            globals()[paramName] = thisPracticeLoop[paramName]
    
    for thisPracticeLoop in PracticeLoop:
        currentLoop = PracticeLoop
        thisExp.timestampOnFlip(win, 'thisRow.t')
        # pause experiment here if requested
        if thisExp.status == PAUSED:
            pauseExperiment(
                thisExp=thisExp, 
                inputs=inputs, 
                win=win, 
                timers=[routineTimer], 
                playbackComponents=[]
        )
        # abbreviate parameter names if possible (e.g. rgb = thisPracticeLoop.rgb)
        if thisPracticeLoop != None:
            for paramName in thisPracticeLoop:
                globals()[paramName] = thisPracticeLoop[paramName]
        
        # --- Prepare to start Routine "trial" ---
        continueRoutine = True
        # update component parameters for each repeat
        # Run 'Begin Routine' code from jitterOnsetPosition
        #list of possible onsets for target
        list_onsets = [1, 1.2, 1.4, 1.6, 1.8]
        
        # randomize these onsets
        shuffle(list_onsets)
        
        #pick the first value from the list 
        onset_trial = list_onsets[0]
        
        #list of possible positions for target
        list_positions = [-0.375, -0.125, 0.125, 0.375]
        
        # randomize these onsets
        shuffle(list_positions)
        
        #pick the first value from the list 
        position_trial = list_positions[0]
        
        #TargetImage is the variable in the Image field of the target_image component
        
        #path of where to find the target image
        if TargetImage == '../../stimuli/shapes/target_square.jpg': #path of where to find the target image
            
            #setting the key press that will be the correct answer
            corrAns = 'v' 
            
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_cross.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'c'
        
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_plus.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'b'
        thisExp.addData('trial.started', globalClock.getTime())
        target_image.setPos((position_trial, 0))
        target_image.setImage(TargetImage)
        keyboard_response.keys = []
        keyboard_response.rt = []
        _keyboard_response_allKeys = []
        # keep track of which components have finished
        trialComponents = [visual_reminder_square, visual_reminder_plus, visual_reminder_cross, left_far_tile, left_mid_tile, right_mid_tile, right_far_tile, target_image, keyboard_response]
        for thisComponent in trialComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "trial" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *visual_reminder_square* updates
            
            # if visual_reminder_square is starting this frame...
            if visual_reminder_square.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                visual_reminder_square.frameNStart = frameN  # exact frame index
                visual_reminder_square.tStart = t  # local t and not account for scr refresh
                visual_reminder_square.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(visual_reminder_square, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'visual_reminder_square.started')
                # update status
                visual_reminder_square.status = STARTED
                visual_reminder_square.setAutoDraw(True)
            
            # if visual_reminder_square is active this frame...
            if visual_reminder_square.status == STARTED:
                # update params
                pass
            
            # *visual_reminder_plus* updates
            
            # if visual_reminder_plus is starting this frame...
            if visual_reminder_plus.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                visual_reminder_plus.frameNStart = frameN  # exact frame index
                visual_reminder_plus.tStart = t  # local t and not account for scr refresh
                visual_reminder_plus.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(visual_reminder_plus, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'visual_reminder_plus.started')
                # update status
                visual_reminder_plus.status = STARTED
                visual_reminder_plus.setAutoDraw(True)
            
            # if visual_reminder_plus is active this frame...
            if visual_reminder_plus.status == STARTED:
                # update params
                pass
            
            # *visual_reminder_cross* updates
            
            # if visual_reminder_cross is starting this frame...
            if visual_reminder_cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                visual_reminder_cross.frameNStart = frameN  # exact frame index
                visual_reminder_cross.tStart = t  # local t and not account for scr refresh
                visual_reminder_cross.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(visual_reminder_cross, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'visual_reminder_cross.started')
                # update status
                visual_reminder_cross.status = STARTED
                visual_reminder_cross.setAutoDraw(True)
            
            # if visual_reminder_cross is active this frame...
            if visual_reminder_cross.status == STARTED:
                # update params
                pass
            
            # *left_far_tile* updates
            
            # if left_far_tile is starting this frame...
            if left_far_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                left_far_tile.frameNStart = frameN  # exact frame index
                left_far_tile.tStart = t  # local t and not account for scr refresh
                left_far_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(left_far_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'left_far_tile.started')
                # update status
                left_far_tile.status = STARTED
                left_far_tile.setAutoDraw(True)
            
            # if left_far_tile is active this frame...
            if left_far_tile.status == STARTED:
                # update params
                pass
            
            # *left_mid_tile* updates
            
            # if left_mid_tile is starting this frame...
            if left_mid_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                left_mid_tile.frameNStart = frameN  # exact frame index
                left_mid_tile.tStart = t  # local t and not account for scr refresh
                left_mid_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(left_mid_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'left_mid_tile.started')
                # update status
                left_mid_tile.status = STARTED
                left_mid_tile.setAutoDraw(True)
            
            # if left_mid_tile is active this frame...
            if left_mid_tile.status == STARTED:
                # update params
                pass
            
            # *right_mid_tile* updates
            
            # if right_mid_tile is starting this frame...
            if right_mid_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                right_mid_tile.frameNStart = frameN  # exact frame index
                right_mid_tile.tStart = t  # local t and not account for scr refresh
                right_mid_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(right_mid_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'right_mid_tile.started')
                # update status
                right_mid_tile.status = STARTED
                right_mid_tile.setAutoDraw(True)
            
            # if right_mid_tile is active this frame...
            if right_mid_tile.status == STARTED:
                # update params
                pass
            
            # *right_far_tile* updates
            
            # if right_far_tile is starting this frame...
            if right_far_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                right_far_tile.frameNStart = frameN  # exact frame index
                right_far_tile.tStart = t  # local t and not account for scr refresh
                right_far_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(right_far_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'right_far_tile.started')
                # update status
                right_far_tile.status = STARTED
                right_far_tile.setAutoDraw(True)
            
            # if right_far_tile is active this frame...
            if right_far_tile.status == STARTED:
                # update params
                pass
            
            # *target_image* updates
            
            # if target_image is starting this frame...
            if target_image.status == NOT_STARTED and tThisFlip >= onset_trial-frameTolerance:
                # keep track of start time/frame for later
                target_image.frameNStart = frameN  # exact frame index
                target_image.tStart = t  # local t and not account for scr refresh
                target_image.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(target_image, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'target_image.started')
                # update status
                target_image.status = STARTED
                target_image.setAutoDraw(True)
            
            # if target_image is active this frame...
            if target_image.status == STARTED:
                # update params
                pass
            
            # if target_image is stopping this frame...
            if target_image.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > target_image.tStartRefresh + 0.2-frameTolerance:
                    # keep track of stop time/frame for later
                    target_image.tStop = t  # not accounting for scr refresh
                    target_image.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_image.stopped')
                    # update status
                    target_image.status = FINISHED
                    target_image.setAutoDraw(False)
            
            # *keyboard_response* updates
            
            # if keyboard_response is starting this frame...
            if keyboard_response.status == NOT_STARTED and t >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                keyboard_response.frameNStart = frameN  # exact frame index
                keyboard_response.tStart = t  # local t and not account for scr refresh
                keyboard_response.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(keyboard_response, 'tStartRefresh')  # time at next scr refresh
                # update status
                keyboard_response.status = STARTED
                # keyboard checking is just starting
                keyboard_response.clock.reset()  # now t=0
                keyboard_response.clearEvents(eventType='keyboard')
            if keyboard_response.status == STARTED:
                theseKeys = keyboard_response.getKeys(keyList=['b','c','v'], ignoreKeys=["escape"], waitRelease=False)
                _keyboard_response_allKeys.extend(theseKeys)
                if len(_keyboard_response_allKeys):
                    keyboard_response.keys = _keyboard_response_allKeys[-1].name  # just the last key pressed
                    keyboard_response.rt = _keyboard_response_allKeys[-1].rt
                    keyboard_response.duration = _keyboard_response_allKeys[-1].duration
                    # was this correct?
                    if (keyboard_response.keys == str(corrAns)) or (keyboard_response.keys == corrAns):
                        keyboard_response.corr = 1
                    else:
                        keyboard_response.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in trialComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "trial" ---
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('trial.stopped', globalClock.getTime())
        # check responses
        if keyboard_response.keys in ['', [], None]:  # No response was made
            keyboard_response.keys = None
            # was no response the correct answer?!
            if str(corrAns).lower() == 'none':
               keyboard_response.corr = 1;  # correct non-response
            else:
               keyboard_response.corr = 0;  # failed to respond (incorrectly)
        # store data for PracticeLoop (TrialHandler)
        PracticeLoop.addData('keyboard_response.keys',keyboard_response.keys)
        PracticeLoop.addData('keyboard_response.corr', keyboard_response.corr)
        if keyboard_response.keys != None:  # we had a response
            PracticeLoop.addData('keyboard_response.rt', keyboard_response.rt)
            PracticeLoop.addData('keyboard_response.duration', keyboard_response.duration)
        # Run 'End Routine' code from compRTandAcc
        #this code is to record the reaction times and accuracy of the trial
        
        thisRoutineDuration = t # how long did this trial last
        
        # keyboard_response.rt is the time with which a key was pressed
        # thisRecRT - how long it took for participants to respond after onset of target_image
        # thisAcc - whether or not the response was correct
        
        # compute RT based on onset of target
        thisRecRT = keyboard_response.rt - onset_trial 
        
        # check of the response was correct
        if keyboard_response.corr == 1: 
        
            # if it was correct, assign 'correct' value
            thisAcc = 'correct' 
        
        # if not, assign 'incorrect' value
        else:
            thisAcc = 'incorrect'
        
        # record the actual response times of each trial 
        thisExp.addData('trialRespTimes', thisRecRT) 
        # the Routine "trial" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        
        # --- Prepare to start Routine "practice_feedback" ---
        continueRoutine = True
        # update component parameters for each repeat
        thisExp.addData('practice_feedback.started', globalClock.getTime())
        feedback_text.setText("The last recorded RT was: "  + str(round(thisRecRT, 3)) + " \nThe response was: "  + thisAcc + " \n\nPress the space bar to continue."  )
        keyboard_response_feedback.keys = []
        keyboard_response_feedback.rt = []
        _keyboard_response_feedback_allKeys = []
        # keep track of which components have finished
        practice_feedbackComponents = [feedback_text, keyboard_response_feedback]
        for thisComponent in practice_feedbackComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "practice_feedback" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *feedback_text* updates
            
            # if feedback_text is starting this frame...
            if feedback_text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                feedback_text.frameNStart = frameN  # exact frame index
                feedback_text.tStart = t  # local t and not account for scr refresh
                feedback_text.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(feedback_text, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'feedback_text.started')
                # update status
                feedback_text.status = STARTED
                feedback_text.setAutoDraw(True)
            
            # if feedback_text is active this frame...
            if feedback_text.status == STARTED:
                # update params
                pass
            
            # *keyboard_response_feedback* updates
            waitOnFlip = False
            
            # if keyboard_response_feedback is starting this frame...
            if keyboard_response_feedback.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                keyboard_response_feedback.frameNStart = frameN  # exact frame index
                keyboard_response_feedback.tStart = t  # local t and not account for scr refresh
                keyboard_response_feedback.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(keyboard_response_feedback, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'keyboard_response_feedback.started')
                # update status
                keyboard_response_feedback.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(keyboard_response_feedback.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(keyboard_response_feedback.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if keyboard_response_feedback.status == STARTED and not waitOnFlip:
                theseKeys = keyboard_response_feedback.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
                _keyboard_response_feedback_allKeys.extend(theseKeys)
                if len(_keyboard_response_feedback_allKeys):
                    keyboard_response_feedback.keys = _keyboard_response_feedback_allKeys[-1].name  # just the last key pressed
                    keyboard_response_feedback.rt = _keyboard_response_feedback_allKeys[-1].rt
                    keyboard_response_feedback.duration = _keyboard_response_feedback_allKeys[-1].duration
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in practice_feedbackComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "practice_feedback" ---
        for thisComponent in practice_feedbackComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('practice_feedback.stopped', globalClock.getTime())
        # check responses
        if keyboard_response_feedback.keys in ['', [], None]:  # No response was made
            keyboard_response_feedback.keys = None
        PracticeLoop.addData('keyboard_response_feedback.keys',keyboard_response_feedback.keys)
        if keyboard_response_feedback.keys != None:  # we had a response
            PracticeLoop.addData('keyboard_response_feedback.rt', keyboard_response_feedback.rt)
            PracticeLoop.addData('keyboard_response_feedback.duration', keyboard_response_feedback.duration)
        # the Routine "practice_feedback" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        thisExp.nextEntry()
        
        if thisSession is not None:
            # if running in a Session with a Liaison client, send data up to now
            thisSession.sendExperimentData()
    # completed 1.0 repeats of 'PracticeLoop'
    
    
    # --- Prepare to start Routine "end_screen" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('end_screen.started', globalClock.getTime())
    end_screen_space_bar.keys = []
    end_screen_space_bar.rt = []
    _end_screen_space_bar_allKeys = []
    # keep track of which components have finished
    end_screenComponents = [end_screen_instructions, end_screen_space_bar]
    for thisComponent in end_screenComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "end_screen" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *end_screen_instructions* updates
        
        # if end_screen_instructions is starting this frame...
        if end_screen_instructions.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            end_screen_instructions.frameNStart = frameN  # exact frame index
            end_screen_instructions.tStart = t  # local t and not account for scr refresh
            end_screen_instructions.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(end_screen_instructions, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'end_screen_instructions.started')
            # update status
            end_screen_instructions.status = STARTED
            end_screen_instructions.setAutoDraw(True)
        
        # if end_screen_instructions is active this frame...
        if end_screen_instructions.status == STARTED:
            # update params
            pass
        
        # *end_screen_space_bar* updates
        waitOnFlip = False
        
        # if end_screen_space_bar is starting this frame...
        if end_screen_space_bar.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            end_screen_space_bar.frameNStart = frameN  # exact frame index
            end_screen_space_bar.tStart = t  # local t and not account for scr refresh
            end_screen_space_bar.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(end_screen_space_bar, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'end_screen_space_bar.started')
            # update status
            end_screen_space_bar.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(end_screen_space_bar.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(end_screen_space_bar.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if end_screen_space_bar.status == STARTED and not waitOnFlip:
            theseKeys = end_screen_space_bar.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _end_screen_space_bar_allKeys.extend(theseKeys)
            if len(_end_screen_space_bar_allKeys):
                end_screen_space_bar.keys = _end_screen_space_bar_allKeys[-1].name  # just the last key pressed
                end_screen_space_bar.rt = _end_screen_space_bar_allKeys[-1].rt
                end_screen_space_bar.duration = _end_screen_space_bar_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in end_screenComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "end_screen" ---
    for thisComponent in end_screenComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('end_screen.stopped', globalClock.getTime())
    # check responses
    if end_screen_space_bar.keys in ['', [], None]:  # No response was made
        end_screen_space_bar.keys = None
    thisExp.addData('end_screen_space_bar.keys',end_screen_space_bar.keys)
    if end_screen_space_bar.keys != None:  # we had a response
        thisExp.addData('end_screen_space_bar.rt', end_screen_space_bar.rt)
        thisExp.addData('end_screen_space_bar.duration', end_screen_space_bar.duration)
    thisExp.nextEntry()
    # the Routine "end_screen" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # mark experiment as finished
    endExperiment(thisExp, win=win, inputs=inputs)


def saveData(thisExp):
    """
    Save data from this experiment
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    """
    filename = thisExp.dataFileName
    # these shouldn't be strictly necessary (should auto-save)
    thisExp.saveAsWideText(filename + '.csv', delim='auto')
    thisExp.saveAsPickle(filename)


def endExperiment(thisExp, inputs=None, win=None):
    """
    End this experiment, performing final shut down operations.
    
    This function does NOT close the window or end the Python process - use `quit` for this.
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    inputs : dict
        Dictionary of input devices by name.
    win : psychopy.visual.Window
        Window for this experiment.
    """
    if win is not None:
        # remove autodraw from all current components
        win.clearAutoDraw()
        # Flip one final time so any remaining win.callOnFlip() 
        # and win.timeOnFlip() tasks get executed
        win.flip()
    # mark experiment handler as finished
    thisExp.status = FINISHED
    # shut down eyetracker, if there is one
    if inputs is not None:
        if 'eyetracker' in inputs and inputs['eyetracker'] is not None:
            inputs['eyetracker'].setConnectionState(False)
    logging.flush()


def quit(thisExp, win=None, inputs=None, thisSession=None):
    """
    Fully quit, closing the window and ending the Python process.
    
    Parameters
    ==========
    win : psychopy.visual.Window
        Window to close.
    inputs : dict
        Dictionary of input devices by name.
    thisSession : psychopy.session.Session or None
        Handle of the Session object this experiment is being run from, if any.
    """
    thisExp.abort()  # or data files will save again on exit
    # make sure everything is closed down
    if win is not None:
        # Flip one final time so any remaining win.callOnFlip() 
        # and win.timeOnFlip() tasks get executed before quitting
        win.flip()
        win.close()
    if inputs is not None:
        if 'eyetracker' in inputs and inputs['eyetracker'] is not None:
            inputs['eyetracker'].setConnectionState(False)
    logging.flush()
    if thisSession is not None:
        thisSession.stop()
    # terminate Python process
    core.quit()


# if running this experiment as a script...
if __name__ == '__main__':
    # call all functions in order
    expInfo = showExpInfoDlg(expInfo=expInfo)
    thisExp = setupData(expInfo=expInfo)
    logFile = setupLogging(filename=thisExp.dataFileName)
    win = setupWindow(expInfo=expInfo)
    inputs = setupInputs(expInfo=expInfo, thisExp=thisExp, win=win)
    run(
        expInfo=expInfo, 
        thisExp=thisExp, 
        win=win, 
        inputs=inputs
    )
    saveData(thisExp=thisExp)
    quit(thisExp=thisExp, win=win, inputs=inputs)
